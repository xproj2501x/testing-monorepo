import { request, APIRequestContext } from '@playwright/test';

export const generateToken = async (): Promise<string> => {
  const context = await request.newContext( {
    baseURL: '',
    extraHTTPHeaders: {

    }
  });
  const response = await context.post('');

  // await context.dispose();
  return response.text();
}

export const clientFactory = async (): Promise<APIRequestContext> => {
  // const token = await generateToken();

  return await request.newContext({
    baseURL: '',
    // extraHTTPHeaders: {
      // 'Authorization': `bearer ${token}`
    // }
  });
};
