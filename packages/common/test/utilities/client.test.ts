import {expect, jest, test} from '@jest/globals';
import { request, APIRequestContext, APIResponse } from '@playwright/test';
import { clientFactory, generateToken } from '../../src/support/utilities';

const mockNewContext = jest.spyOn(request, 'newContext');
const mockContext = {
  post: (x: string): Promise<APIResponse> =>  Promise.resolve({
    text: (): Promise<string> => Promise.resolve(x)
  } as APIResponse),
  dispose: (): Promise<void> => Promise.resolve()
} as APIRequestContext;

mockNewContext.mockReturnValue(
  Promise.resolve(mockContext)
);

test('Generate API token', async (): Promise<void> => {
  await generateToken();

  expect(request.newContext).toHaveBeenCalled();
});

test('Should create API client', async (): Promise<void> => {

  await clientFactory();
  expect(request.newContext).toHaveBeenCalled();
});