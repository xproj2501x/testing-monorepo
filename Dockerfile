FROM node:current-slim
RUN apt-get update && apt install git -y
COPY ./ .
RUN yarn install
